﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using consoleAssignment_5;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace consoleAssignment_5.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        //[TestMethod()]
        //public void MainTest()
        //{
        //    Assert.Fail();
        //}

        [TestMethod]
        public void TestMethodInt1()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase1.exe " + @"C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase1.cs";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
//            string compileResult = p.StandardOutput.ReadToEnd();
//            Console.WriteLine(compileResult);
////            Assert.AreEqual(compileResult, "pass");

            p.StartInfo.FileName = @"C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase1.exe";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadLine();

            Assert.AreEqual(result, "pass");
        }


        [TestMethod]
        public void TestMethodInt2()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase2.exe " + @"C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase2.cs";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();

            p.StartInfo.FileName = @"C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase2.exe";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadLine();

            Assert.AreEqual(result, "pass");
        }

        [TestMethod]
        public void TestMethodInt3()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase3.exe " + @"C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase3.cs";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();

            p.StartInfo.FileName = @"C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase3.exe";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadLine();

            Assert.AreEqual(result, "pass");
        }

        [TestMethod]
        public void TestMethodInt4()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase4.exe " + @"C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase4.cs";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            

            p.StartInfo.FileName = @"C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase4.exe";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadLine();

            Assert.AreEqual(result, "pass");
        }
        [TestMethod]
        public void TestMethodInt5()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase5.exe " + @"C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase5.cs";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            

            p.StartInfo.FileName = @"C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase5.exe";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadLine();

            Assert.AreEqual(result, "pass");
        }

        [TestMethod]
        public void TestMethodIntNegative()
        {
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe";
            p.StartInfo.Arguments = @"/out:C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCase5.exe " + @"C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\bitbucket\Assignment_5\testCaseNegative.cs";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();


            string compileResult = p.StandardOutput.ReadToEnd();
//            Console.WriteLine(compileResult);
            Assert.IsTrue(compileResult.Contains("error CS0037"));

        }

    }
}