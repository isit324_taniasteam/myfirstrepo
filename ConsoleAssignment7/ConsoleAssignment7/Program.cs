﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleAssignment7
{
    class Program
    {
        static void Main(string[] args)
        {

            //f(int.MaxValue);
             f(int.MaxValue);
            Console.ReadLine();
        }
        static long f(int n)
        {

            long sum = 0;
            long averageTime = 0;
            for (var j = 1; j <= 5; j++)
            {
                sum = 0;
                long sum1 = 0;
                long sum2 = 0;
                long sum3 = 0;
                Stopwatch sw = new Stopwatch();
                sw.Start();
                sum1 = (n - (n % 3)) / 3;
                sum1 = (sum1 * (sum1 + 1)) / 2 * 3;
                sum2 = (n - (n % 5)) / 5;
                sum2 = (sum2 * (sum2 + 1)) / 2 * 5;
                sum3 = (n - (n % 15)) / 15;
                sum3 = (sum3 * (sum3 + 1)) / 2 * 15;
                sum = sum1 + sum2 - sum3;
                sw.Stop();
                Console.WriteLine("Sum: " + sum);
                Console.WriteLine("Each: " + sw.ElapsedTicks);
                averageTime += sw.ElapsedTicks;
                Console.WriteLine("Average: " + averageTime / j);
                Console.WriteLine();
            }

            return sum;
        }
        //static long f(int n)
        //{
        //    long sum = 0;
        //    long averageTime = 0;
        //    for (var j = 1; j <= 5; j++)
        //    {
        //        Stopwatch sw = new Stopwatch();
        //        sw.Start();
        //        bool calculateFive = true;
        //        for (int i = 1; i <= n / 3; i++)
        //        {
        //            sum += i * 3;
        //            if (calculateFive)
        //            {
        //                if (n / 5 < i)
        //                {
        //                    calculateFive = false;
        //                }
        //                int multipleFive = i * 5;
        //                if (multipleFive % 3 != 0)
        //                {
        //                    sum += multipleFive;
        //                }
        //            }
        //        }

        //        sw.Stop();
        //        Console.WriteLine("Each: " + sw.ElapsedMilliseconds);
        //        averageTime += sw.ElapsedMilliseconds;
        //        Console.WriteLine("Sum: " + sum / j);
        //        Console.WriteLine("Average: " + averageTime / j);
        //    }

        //    return sum;
        //}
    }
}
