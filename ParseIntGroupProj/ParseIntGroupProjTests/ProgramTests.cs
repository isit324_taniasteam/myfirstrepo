﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ParseIntGroupProj;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Automation;

namespace ParseIntGroupProj.Tests
{
    [TestClass()]
    public class ProgramTests
    {

        private DateTime testDate;

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ParseTestWithNull()
        {
            Program.Parse(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithZeroYear()
        {
            Program.Parse("01/30/0000");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithZeroMonth()
        {
            Program.Parse("00/30/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithZeroDay()
        {
            Program.Parse("01/00/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithJan32()
        {
            Program.Parse("01/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithFeb30()
        {
            Program.Parse("02/30/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithMar32()
        {
            Program.Parse("03/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithApr31()
        {
            Program.Parse("04/31/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithMay32()
        {
            Program.Parse("05/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithJun31()
        {
            Program.Parse("06/31/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithJul32()
        {
            Program.Parse("07/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithAug32()
        {
            Program.Parse("08/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithSep31()
        {
            Program.Parse("09/31/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithOct32()
        {
            Program.Parse("10/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithNov32()
        {
            Program.Parse("11/31/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithDec32()
        {
            Program.Parse("12/32/2015");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTestWithMonth13()
        {
            Program.Parse("13/01/2015");
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTest12()
        {
            Program.Parse("12/31/10000");

        }
        [TestMethod()]
        public void ParseTest13()
        {

            Assert.AreEqual(Program.Parse("1/10/9998"), DateTime.Parse("1/10/9998"));
        }
        [TestMethod()]
        public void ParseTest14()
        {
            // DateTime result = Program.Parse("12/31/2015");
            Assert.AreEqual(Program.Parse("12/31/2015"), DateTime.Parse("12 / 31 / 2015"));
        }
        [TestMethod()]
        public void ParseTest15()
        {
            Assert.AreEqual(Program.Parse("11/30/2015"), DateTime.Parse("11 / 30 / 2015"));
        }
        [TestMethod()]
        public void ParseTest16()
        {
            Assert.AreEqual(Program.Parse("10/31/2015"), DateTime.Parse("10/31/2015"));
        }
        [TestMethod()]
        public void ParseTest17()
        {
            Assert.AreEqual(Program.Parse("09/30/2015"), DateTime.Parse("09/30/2015"));
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTest()
        {
            Program.Parse("1//00000");
        }
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ParseTest1()
        {
            Program.Parse("1/1/0000");
        }
        [TestMethod()]
        public void ParseTest2()
        {
            Assert.AreEqual(Program.Parse("01/01/2015"), DateTime.Parse("01/01/2015"));
        }
        [TestMethod()]
        public void ParseTest3()
        {
            Assert.AreEqual(Program.Parse("01/31/2015"), (testDate = new DateTime(2015, 1, 31)));
        }
        [TestMethod()]
        public void ParseTest4()
        {
            Assert.AreEqual(Program.Parse("02/28/2015"), (testDate = new DateTime(2015, 2, 28)));
        }
        [TestMethod()]
        public void ParseTest5()
        {
            Assert.AreEqual(Program.Parse("03/31/2015"), (testDate = new DateTime(2015, 3, 31)));
        }
        [TestMethod()]
        public void ParseTest6()
        {
            Assert.AreEqual(Program.Parse("04/30/2015"), (testDate = new DateTime(2015, 4, 30)));
        }
        [TestMethod()]
        public void ParseTest7()
        {
            Assert.AreEqual(Program.Parse("05/31/2015"), (testDate = new DateTime(2015, 5, 31)));
        }
        [TestMethod()]
        public void ParseTest8()
        {
            Assert.AreEqual(Program.Parse("06/30/2015"), (testDate = new DateTime(2015, 6, 30)));
        }
        [TestMethod()]
        public void ParseTest9()
        {
            Assert.AreEqual(Program.Parse("07/31/2015"), (testDate = new DateTime(2015, 7, 31)));
        }
        [TestMethod()]
        public void ParseTest10()
        {
            Assert.AreEqual(Program.Parse("08/31/2015"), (testDate = new DateTime(31, 8, 2015)));
        }
        [TestMethod()]
        public void ParseTest11()
        {
            Assert.AreEqual(Program.Parse("09/30/2015"), (testDate = new DateTime(2015, 9, 30)));
        }
    }


}