﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using finalTestingWithMarcelo_TaniaQ6;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace finalTestingWithMarcelo_TaniaQ6.Tests
{
    [TestClass()]
    public class ProgramTests
    {


        [TestMethod]
        public void TestMethodInt1()
        {
            Stopwatch sw = new Stopwatch();

            sw.Start();
            Process p = new Process();

            p.StartInfo.FileName = @"C:\Users\vendr_000\Desktop\Tania\COLLEGE\WINTER_2016\isit324\CalculateRate\CalculateRate.exe";
            p.StartInfo.Arguments = @"100000 10 5";
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            p.Start();
            p.WaitForExit();
            string result = p.StandardOutput.ReadToEnd().Trim();
           //Console.WriteLine(result);
            Assert.AreEqual(result, "1,060.66");
            sw.Stop();
            Console.WriteLine("time taken: " + sw.ElapsedMilliseconds);
        }
    }
}