﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Diagnostics;

namespace finalTestingWithMarcelo_Tania
{
    class Program
    {
        static void Main(string[] args)
        {
            //amount = 100000, term = 10, rate = 5%==0.05

            double apr = 0.05;
            double numOfPmnts = 10 * 12;
            double loanAmount = 100000;
            double expected = 1060.66;
            Stopwatch sw = new Stopwatch();

            sw.Start();
            double result = Financial.Pmt((apr / 12), numOfPmnts, loanAmount) * -1;
            sw.Stop();
            Console.WriteLine("time taken: " + sw.ElapsedTicks);

            Console.WriteLine("result: " + result);
            Console.WriteLine("expected: " + expected);
            Console.ReadLine();
        }

    }
}
